# NixOS Dotfiles

Set your machine hostname in `ident.nix` and set your passwords and other secrets in `/users/secrets.nix`, see the examples for help.
